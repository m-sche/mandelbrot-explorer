#include <SDL2/SDL.h>
#include <png.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>

/* Screen size (multiple of six when you want a sharp line through a+0*i) */
#define SCR_W	1000
#define SCR_H	650
#define DIM	(SCR_H > SCR_W ? SCR_W : SCR_H)

/* Colors or boundaries in debug lists */
#define DBG_IN	"M: "
#define DBG_OUT	"\n"

/* Powers of 2, do not change */
#define EV_ZOOM		1
#define EV_SHIFT	2
#define EV_QUALITY	4

/* Integer infinity */
#define INF	INT_MAX

/* Rendering matrix -- the sequence of screen chunks */
#define MATRIX_LIMIT	5
int render_matrix[MATRIX_LIMIT][4] = {
	{0.27*SCR_W,	0.27*SCR_H,	0.46*SCR_W,	0.46*SCR_H},
	{0.18*SCR_W,	0.18*SCR_H,	0.64*SCR_W,	0.64*SCR_H},
	{0.11*SCR_W,	0.11*SCR_H,	0.78*SCR_W,	0.78*SCR_H},
	{0.05*SCR_W,	0.05*SCR_H,	0.90*SCR_W,	0.90*SCR_H},
	{0,		0,	 	SCR_W,		SCR_H},
};

/* Configuration */
#define DENSITY		255	/* Color density (more means more colorful) */
#define SHIFT_FACTOR	46	/* Shift [px] caused by a keystroke */
#define MIN_ITER_INC	60	/* Increase of min_iter when zoomed */
#define MAX_ITER_INC	100	/* Increase of max_iter when zoomed */
#define APPROX		3	/* Draw first squares of this side length */

/* Initial values */
#define INIT_ITER	120
#define INIT_ZOOM	1
#define INIT_RE		-1.75
#define INIT_IM		-1.5

struct future_shift {
	int x;
	int y;
} future_shift;

struct pixel {
	int iter;
	int value;
	long double z_re;
	long double z_im;
};

struct color_scheme {
	Uint32 (*pick)();
	struct color_scheme *next;
};

int min_iter = INIT_ITER;
int max_iter = INIT_ITER;
double zoom = INIT_ZOOM;
long double re_shift = INIT_RE*SCR_W/DIM;
long double im_shift = INIT_IM*SCR_H/DIM;

SDL_Surface *screen;

struct pixel **pixel;
struct color_scheme color_scheme;

/* Reset iterations of all pixels to zero */
static inline void reset_pixel_iter(void)
{
	int x, y;

	for (y = 0; y < SCR_H; y++)
		for (x = 0; x < SCR_W; x++)
			pixel[x][y].iter = 0;
}

/* Allocate a 2D array of pixels */
struct pixel **alloc_pixel_array(int w, int h)
{
	struct pixel **array;

	array = (struct pixel **)malloc(w*sizeof(struct pixel *));
	if (array == NULL)
		exit(1);

	while (w--) {
		array[w] = (struct pixel *)calloc(h, sizeof(struct pixel));
		if (array[w] == NULL)
			exit(1);
	}

	return array;
}

/* Free allocated 2D array of pixels */
void free_pixel_array(struct pixel **array, int w)
{
	while (w)
		free(array[--w]);
	free(array);
}

/* Simple and approximate log2 function */
int alog2(double num)
{
	int exp = 1;
	while (num > 1 && exp++)
		num /= 2;
	return exp;
}

/* Declare new color scheme */
#define CHAIN_COLOR(prev, new, foll)	\
	struct color_scheme new;	\
	new.pick = pick_##new;		\
	prev.next = &new;		\
	new.next = &foll;

/* Assign a color to an integer value */
Uint32 pick_color(int d)
{
	return color_scheme.pick(d%256);
}

/* Shift the color scheme ring */
void change_color_scheme(void)
{
	color_scheme = *color_scheme.next;
}

/* Coloring algorithms (must have prefix pick_) */

Uint32 pick_cs_blue(int d)
{
	switch((int)(3*d/256)) {
	case 0: return SDL_MapRGB(screen->format, 3*d, 0, 127-d/2);
	case 1: return SDL_MapRGB(screen->format, 255, 3*d-255, 127-d/2);
	case 2: return SDL_MapRGB(screen->format, 255-3*d, 511-3*d, 127-d/2);
	default: return 0;
	}
}

Uint32 pick_cs_yellow(int d)
{
	switch((int)(4*d/256)) {
	case 0: return SDL_MapRGB(screen->format, 255, 255-4*d, d);
	case 1: return SDL_MapRGB(screen->format, 255-4*d, 0, d);
	case 2: return SDL_MapRGB(screen->format, 0, 4*d, 255-d);
	case 3: return SDL_MapRGB(screen->format, 4*d, 255, 255-d);
	default: return 0;
	}
}

Uint32 pick_cs_milk(int d)
{
	switch((int)(3*d/256)) {
	case 0: return SDL_MapRGB(screen->format, 255-2*d, 255-2*d, 255-2*d);
	case 1: return SDL_MapRGB(screen->format, 2*d-85, 170-d, 170-d);
	case 2: return SDL_MapRGB(screen->format, 3*(255-d), 0, 0);
	default: return 0;
	}
}

/* Colorize the pixel */
static inline void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
	Uint32 *ptr = (Uint32 *)surface->pixels;
	ptr[y*(surface->pitch/4)+x] = pixel;
}

/* Read and return the pixel from surface */
static inline Uint32 readpixel(SDL_Surface *surface, int x, int y)
{
	Uint32 *ptr = (Uint32 *)surface->pixels;
	Uint32 color = ptr[y*(surface->pitch/4)+x];
	return color;
}

/*
 * If the point is a part of the Mandelbrot set, return zero, else return
 * the number of iterations needed to prove the opposite
 */
static inline struct pixel is_mandelbrot(struct pixel old, double c_re, double c_im, int iter)
{
	int i;
	double z_re = old.z_re;
	double z_im = old.z_im;
	double z_re_square;
	double z_im_square;
	struct pixel pixel;

	for (i = old.iter; i <= iter; i++) {
		z_re_square = z_re*z_re;
		z_im_square = z_im*z_im;
		if (z_re_square + z_im_square > 4) {
			pixel.iter = INF;
			pixel.value = i;
			return pixel;
		}
		z_im = 2*z_re*z_im+c_im;
		z_re = z_re_square-z_im_square+c_re;
	}
	pixel.iter = iter;
	pixel.value = -1;
	pixel.z_re = z_re;
	pixel.z_im = z_im;
	return pixel;
}

/* Draw a little white cross */
void render_cursor(SDL_Surface *surface, int x, int y)
{
	int i;
	Uint32 white = SDL_MapRGB(screen->format, 255, 255, 255);

	if (SDL_MUSTLOCK(surface))
		SDL_LockSurface(surface);
	for (i = x-3; i <= x+3 && i < SCR_W; i++)
		putpixel(surface, i, y, white);
	for (i = y-3; i <= y+3 && i < SCR_H; i++)
		putpixel(surface, x, i, white);
	if (SDL_MUSTLOCK(surface))
		SDL_UnlockSurface(surface);
}

/* Render pixel array on the screen */
void render_mandelbrot(SDL_Surface *surface, int st_x, int st_y, int w, int h)
{
	int x, y;

	if (SDL_MUSTLOCK(surface))
		SDL_LockSurface(surface);

	for (y = st_y; y < st_y+h; y++) {
		for (x = st_x; x < st_x+w; x++) {
			Uint32 color;
			if (pixel[x][y].value == -1)
				color = 0;
			else
				color = pick_color(DENSITY*pixel[x][y].value/max_iter);
			putpixel(surface, x, y, color);
		}
	}

	if (SDL_MUSTLOCK(surface))
		SDL_UnlockSurface(surface);
}

/* Compute a single pixel of the Mandelbrot set */
void compute_pixel(int x, int y, int iter)
{
	const double pix_to_double = (double)3/DIM/zoom;

	if (pixel[x][y].iter == 0) {
		pixel[x][y].z_re = (double)x*pix_to_double+re_shift;
		pixel[x][y].z_im = (double)y*pix_to_double+im_shift;
	}
	pixel[x][y] = is_mandelbrot(
		pixel[x][y],
		(double)x*pix_to_double+re_shift,
		(double)y*pix_to_double+im_shift,
		iter);
}

/* Compute the Mandelbrot set */
void compute_mandelbrot(int st_x, int st_y, int w, int h, int iter, int approx)
{
	int y, x;
	int i, j;

	for (y = st_y; y < st_y+h; y += approx) {
		for (x = st_x; x < st_x+w; x += approx) {
			if (pixel[x][y].iter < iter)
				compute_pixel(x, y, iter);
			for (i = 0; i < approx && x+i < SCR_W; i++)
				for (j = 0; j < approx && y+j < SCR_H; j++)
					if (pixel[x+i][y+j].iter < iter)
						pixel[x+i][y+j].value = pixel[x][y].value;
		}
	}
}

/* Shift the Mandelbrot set */
void shift_mandelbrot(void)
{
	struct pixel **new_pixel;
	int y, x;

	new_pixel = alloc_pixel_array(SCR_W, SCR_H);
	if (new_pixel == NULL)
		exit(1);

	for (y = 0; y < SCR_H; y++)
		for (x = 0; x < SCR_W; x++)
			if (x+future_shift.x >= 0 && x+future_shift.x < SCR_W &&
			y+future_shift.y >= 0 && y+future_shift.y < SCR_H)
				new_pixel[x+future_shift.x][y+future_shift.y] = pixel[x][y];

	for (y = 0; y < SCR_H; y++)
		for (x = 0; x < SCR_W; x++)
			pixel[x][y] = new_pixel[x][y];

	future_shift.x = 0;
	future_shift.y = 0;

	free_pixel_array(new_pixel, SCR_W);
}

void quit (int val)
{
	free_pixel_array(pixel, SCR_W);
	SDL_Quit();
	exit(val);
}

/* Save current screen as a png file */
void save_screenshot(void)
{
	FILE *file;
	png_structp png_ptr;
	png_infop info_ptr;
	png_bytep row;
	char filename[40];
	int x, y;
	int file_no = 1;

	do
		snprintf(filename, 40, "screenshot-%d.png", file_no++);
	while (access(filename, F_OK) == 0);

	file = fopen(filename, "wb");
	png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	info_ptr = png_create_info_struct(png_ptr);

	if (!file || !png_ptr || !info_ptr)
		return;

	png_init_io(png_ptr, file);
	png_set_IHDR(png_ptr, info_ptr, SCR_W, SCR_H, 8, PNG_COLOR_TYPE_RGB,
		PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
	png_write_info(png_ptr, info_ptr);

	row = (png_bytep)malloc(3*SCR_W*sizeof(png_byte));

	if (SDL_MUSTLOCK(screen))
		SDL_LockSurface(screen);

	for (y = 0; y < SCR_H; y++) {
		for (x = 0; x < SCR_W; x++) {
			Uint32 color = readpixel(screen, x, y);
			SDL_GetRGB(color, screen->format,
				(Uint8 *)&row[x*3],
				(Uint8 *)&row[x*3+1],
				(Uint8 *)&row[x*3+2]);
		}
		png_write_row(png_ptr, row);
	}

	if (SDL_MUSTLOCK(screen))
		SDL_UnlockSurface(screen);

	png_write_end(png_ptr, NULL);

	fclose(file);
	free(row);
	png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
	png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
}

/* Process caught keyborad events */
int process_keydown(SDL_Keycode code)
{
	int change = 0;

	switch (code) {
	case SDLK_ESCAPE:
	case SDLK_q:
		quit(0);
	case SDLK_p:
		/* Reset to initial state */
		zoom = 1;
		re_shift = INIT_RE*SCR_W/DIM;
		im_shift = INIT_IM*SCR_H/DIM;
		min_iter = INIT_ITER;
		max_iter = INIT_ITER;
		change |= EV_ZOOM;
		break;
	case SDLK_u:
		/* Zoom in */
		zoom *= 2;
		re_shift += 1.5*SCR_W/DIM/zoom;
		im_shift += 1.5*SCR_H/DIM/zoom;
		min_iter += zoom > 1 ? MIN_ITER_INC : 0;
		max_iter += zoom > 1 ? MAX_ITER_INC : 0;
		change |= EV_ZOOM;
		break;
	case SDLK_o:
		/* Zoom out */
		zoom /= 2;
		re_shift -= 0.75*SCR_W/DIM/zoom;
		im_shift -= 0.75*SCR_H/DIM/zoom;
		min_iter -= zoom >= 1 && min_iter > MIN_ITER_INC ? MIN_ITER_INC : 0;
		max_iter -= zoom >= 1 && max_iter > MAX_ITER_INC ? MAX_ITER_INC : 0;
		change |= EV_ZOOM;
		break;
	case SDLK_i:
		/* Move up */
		im_shift -= (double)3*SHIFT_FACTOR/DIM/zoom;
		future_shift.y += SHIFT_FACTOR;
		change |= EV_SHIFT;
		break;
	case SDLK_j:
		/* Move left */
		re_shift -= (double)3*SHIFT_FACTOR/DIM/zoom;
		future_shift.x += SHIFT_FACTOR;
		change |= EV_SHIFT;
		break;
	case SDLK_k:
		/* Move down */
		im_shift += (double)3*SHIFT_FACTOR/DIM/zoom;
		future_shift.y -= SHIFT_FACTOR;
		change |= EV_SHIFT;
		break;
	case SDLK_l:
		/* Move right */
		re_shift += (double)3*SHIFT_FACTOR/DIM/zoom;
		future_shift.x -= SHIFT_FACTOR;
		change |= EV_SHIFT;
		break;
	case SDLK_z:
		/* Increase quality */
		min_iter += MIN_ITER_INC;
		max_iter += MAX_ITER_INC;
		change |= EV_QUALITY;
		break;
	case SDLK_x:
		/* Decrease quality */
		min_iter -= min_iter > MIN_ITER_INC ? MIN_ITER_INC : 0;
		max_iter -= max_iter > MAX_ITER_INC ? MAX_ITER_INC : 0;
		change |= EV_QUALITY;
		break;
	case SDLK_c:
		/* Change color scheme */
		change_color_scheme();
		change |= EV_QUALITY;
		break;
	case SDLK_s:
		/* Make a screenshot */
		compute_mandelbrot(0, 0, SCR_W, SCR_H, max_iter, 1);
		render_mandelbrot(screen, 0, 0, SCR_W, SCR_H);
		save_screenshot();
		break;
	default:
		break;
	}

	return change;
}

/* Process caught keyborad or mouse events */
int process_events(void)
{
	SDL_Event event;
	int change = 0;

	while (SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_QUIT:
			quit(0);
		case SDL_KEYDOWN:
			change |= process_keydown(event.key.keysym.sym);
		}
	}
	if (change & EV_SHIFT)
		printf(DBG_IN "Re [%+.8Lf], Im [%+.8Lf]" DBG_OUT,
			re_shift+1.5/zoom,
			im_shift+1.5/zoom);
	if (change & (EV_ZOOM | EV_QUALITY))
		printf(DBG_IN "Zoom [%d], Iter [%d/%d]" DBG_OUT,
			alog2(zoom), min_iter, max_iter);
	return change;
}

void update_screen(SDL_Renderer *renderer, SDL_Surface *surface)
{
	static SDL_Texture *texture = NULL;
	
	if (texture == NULL)
		texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888,
			SDL_TEXTUREACCESS_STATIC, SCR_W, SCR_H);

	SDL_UpdateTexture(texture, NULL, surface->pixels, surface->pitch);
	SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer, texture, NULL, NULL);
	SDL_RenderPresent(renderer);
}

void press_key(SDL_Keycode code)
{
	SDL_Event event;
	event.type = SDL_KEYDOWN;
	event.key.keysym.sym = code;
	SDL_PushEvent(&event);
}

int main(int argc, char *argv[])
{
	pixel = alloc_pixel_array(SCR_W, SCR_H);

	SDL_Init(SDL_INIT_VIDEO);
	SDL_Window *window = SDL_CreateWindow("Mandelbrot set browser",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCR_W, SCR_H, 0);
	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);
	screen = SDL_CreateRGBSurface(0, SCR_W, SCR_H, 32,
		0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);

	SDL_Event event;
	int change;
	int approx;
	int iter;
	int chunk;

	CHAIN_COLOR(cs_blue, cs_blue, cs_blue);
	CHAIN_COLOR(cs_blue, cs_yellow, cs_blue);
	CHAIN_COLOR(cs_blue, cs_milk, cs_yellow);
	color_scheme = cs_milk;

	press_key(SDLK_p);
	while (1) {
		SDL_WaitEvent(&event);
		SDL_PushEvent(&event);
		change = process_events();
		while (change) {
			if (change & EV_ZOOM) {
				reset_pixel_iter();
				future_shift.x = 0;
				future_shift.y = 0;
			} else if (change & EV_SHIFT) {
				shift_mandelbrot();
			}

			approx = APPROX;
			iter = min_iter;
			chunk = 0;

			compute_mandelbrot(0, 0, SCR_W, SCR_H, iter, approx);
			render_mandelbrot(screen, 0, 0, SCR_W, SCR_H);
			render_cursor(screen, SCR_W/2, SCR_H/2);
			update_screen(renderer, screen);

			change = process_events();
			while (change == 0 && chunk < MATRIX_LIMIT) {
				int *m = render_matrix[chunk];

				compute_mandelbrot(m[0], m[1], m[2], m[3], iter, approx);
				render_mandelbrot(screen, m[0], m[1], m[2], m[3]);
				render_cursor(screen, SCR_W/2, SCR_H/2);
				update_screen(renderer, screen);

				chunk++;

				iter += (max_iter-min_iter)/(APPROX*MATRIX_LIMIT);
				if (chunk == MATRIX_LIMIT && approx > 1) {
					chunk = 0;
					approx--;
				}

				change = process_events();
			}
		}
	}
}
