# mandelbrot-explorer

Interactive explorer of the Mandelbrot set, including the common colorization of the area outside of the set.

## Navigation

- `q` – Quit
- `p` – Reset view
- `u` – Zoom in
- `o` – Zoom out
- `i`, `j`, `k`, `l` – Move view
- `z` – Increase iterations (quality)
- `x` – Decrease iterations (quality)
- `c` – Cycle color scheme
- `s` – Make a png screenshot

## Dependencies

- SDL2 development library
- PNG library
