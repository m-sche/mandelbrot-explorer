CC	= gcc
CFLAGS	= -Wall -O2 # -pg -fprofile-arcs -ftest-coverage
LDFLAGS	= -lSDL2 -lpng

all: mandelbrot

mandelbrot: mandelbrot.c
	$(CC) $(CFLAGS) mandelbrot.c -o $@ $(LDFLAGS)
